require 'omniauth/strategies/oauth2'

module OmniAuth
  module Strategies
    class Srichand < OmniAuth::Strategies::OAuth2
      # change the class name and the :name option to match your application name
      option :name, :srichand

      option :client_options, {
        :site => "http://shop.srichand.co.th",
        #:site => "http://localhost:3000",
        :authorize_url => "/oauth/authorize"
      }

      uid { raw_info["id"].to_s }

      info do
        {
          :email => raw_info["email"]
          # and anything else you want to return to your API consumers
        }
      end

      def raw_info
        @raw_info ||= access_token.get('/api/me.json').parsed
      end
    end
  end
end
